import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';
import { AppComponent } from './app.component';
import { HeaderComponent } from './components/header/header.component';
import { AlertpageComponent } from './components/alertpage/alertpage.component';
import { Routes, RouterModule } from '@angular/router';
import { QrCodeComponent } from './components/qr-code/qr-code.component';
import { QRCodeModule } from 'angularx-qrcode';
import {NgbModule} from '@ng-bootstrap/ng-bootstrap';
import { BlockUIModule } from 'ng-block-ui';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
const routes: Routes = [
  {path: '', component: AlertpageComponent},

];


@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    AlertpageComponent,
    QrCodeComponent
  ],
  imports: [
    BrowserModule,
    QRCodeModule,
    NgbModule,
    HttpClientModule,
    FormsModule,
    BlockUIModule.forRoot(),
    ReactiveFormsModule,
    RouterModule.forRoot(routes)
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
