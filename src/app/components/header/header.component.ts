import { Component, OnInit } from '@angular/core';
import { AppConstant } from 'src/app/constants/AppConstant';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {

  header = ''
  date
  constructor() { }

  ngOnInit() {
    this.date = new Date()
    this.header = AppConstant.webAppName
  }

}
