import { Component, OnInit, ViewChild, ElementRef } from "@angular/core";
import { AppConstant } from "src/app/constants/AppConstant";
import { NgbModal } from "@ng-bootstrap/ng-bootstrap";
import {
  HttpClient,
  HttpHeaders,
  HttpErrorResponse,
} from "@angular/common/http";
import { NgBlockUI, BlockUI } from "ng-block-ui";
import * as moment from "moment";
import { catchError } from "rxjs/operators";
import { Observable } from "rxjs";

@Component({
  selector: "app-alertpage",
  templateUrl: "./alertpage.component.html",
  styleUrls: ["./alertpage.component.css"],
})
export class AlertpageComponent implements OnInit {
  percentage = 0;
  alertPercentage;
  waterColor;
  selectedDevice;
  alerts = [];
  devices = [];
  Fill_Status_Critical;
  Fill_Status_Red;
  Fill_Status_Yellow;
  @BlockUI() blockUI: NgBlockUI;
  @ViewChild("content") deviceListModal: ElementRef;
  deviceData;
  manualAlert: any;
  deviceId: any;
  constructor(private modalService: NgbModal, private http: HttpClient) {}

  ngAfterViewInit() {}
  ngOnInit() {
    const headers = new HttpHeaders({
      Authorization: "Basic YWRtaW46T0RBek1rNXZkbUZUWTI5MGFXRT0=",
    });
    this.blockUI.start("Loading devices...");
    this.http
      .post(
        "https://cloudfeedgrain-dev.mybluemix.net/device/get_devices",
        {},
        { headers }
      )
      .subscribe((data) => {
        this.devices = data["docs"];
        this.blockUI.stop();
      });

    this.percentage = (this.alertPercentage / 100) * 300;
    // this.waterColor = this.findColor();
    // this.populateAlerts()
  }
  // populateAlerts() {
  //   this.alerts.push(
  //     {date: new Date(), message: 'Grain Level is Yellow '},
  //     {date: new Date(), message: 'Grain Level is Yellow '},
  //     {date: new Date(), message: 'Grain Level is Yellow '},
  //     {date: new Date(), message: 'Grain Level is Yellow '}
  //   )
  // }

  open(content) {
    this.modalService.open(content);
  }

  getAlerts(deviceId) {
    this.blockUI.start("Loading alerts...");
    const headers = new HttpHeaders({
      Authorization: "Basic YWRtaW46T0RBek1rNXZkbUZUWTI5MGFXRT0=",
    });
    this.http
      .post(
        "https://cloudfeedgrain-dev.mybluemix.net/device/get_alerts",
        { device_id: [deviceId.split(":")[1].trim()].join("") },
        { headers }
      )
      .subscribe((data: any) => {
        this.alerts = data;
        this.blockUI.stop();
        const date = new Date();
      });
  }

  Math: any;
  Constructor() {
    this.Math = Math;
  }

  getInfo(deviceId) {
    this.blockUI.start("Loading device info...");
    const headers = new HttpHeaders({
      Authorization: "Basic YWRtaW46T0RBek1rNXZkbUZUWTI5MGFXRT0=",
    });
    this.http
      .post(
        "https://cloudfeedgrain-dev.mybluemix.net/device/get_info",
        { device_id: deviceId.split(":")[1].trim() },
        { headers }
      )
      .pipe(
        catchError((err: HttpErrorResponse) => {
          this.blockUI.stop();
          this.deviceData = undefined;
          alert("Error while fetching data");
          return Observable.throw(err);
        })
      )
      .subscribe((data: any) => {
        data.projectedStart = moment
          .unix(
            data.start_of_cycle_date_time_int +
              data.projected_seconds_from_start_int
          )
          .format("D-MMM-YYYY hh:mm:ss")
          .toString();
        data.spanMin = moment
          .unix(
            data.start_of_cycle_date_time_int + data.projected_seconds_span_int
          )
          .format("D-MMM-YYYY hh:mm:ss")
          .toString();
        data.spanMax = moment
          .unix(
            data.start_of_cycle_date_time_int - data.projected_seconds_span_int
          )
          .format("D-MMM-YYYY hh:mm:ss")
          .toString();
        data.percent_motor_remaining = Math.floor(data.percent_motor_remaining);
        data.projected_run_out_time_int = Math.round(
          data.projected_run_out_time_int
        );

        this.deviceData = data;
        this.blockUI.stop();
        this.percentage = (data.percent / 100) * 300 + 40;
        this.alertPercentage = Math.round(data.percent);
        // this.waterColor = this.findColor();
        this.Fill_Status_Critical = data.Fill_Status_Critical;
        this.Fill_Status_Red = data.Fill_Status_Red;
        this.Fill_Status_Yellow = data.Fill_Status_Yellow;
        console.log(data);
      });

    this.getAlerts(deviceId);
    this.deviceId = deviceId;
  }

  // [class.good-status]="deviceData?.percent_motor_remaining > 60"
  // [class.good-status]="deviceData?.percent_motor_remaining < 60"
  // [class.good-status]="deviceData?.percent_motor_remaining < 40"
  // [class.good-status]="deviceData?.percent_motor_remaining < 10"

  // findColor() {
  //   if (
  //     this.Fill_Status_Critical !== 0 &&
  //     this.Fill_Status_Critical <= this.deviceData.percent_motor_remaining
  //   ) {
  //     return Black;
  //   }
  //   if (
  //     this.Fill_Status_Red !== 0 &&
  //     this.Fill_Status_Red <= this.deviceData.percent_motor_remaining
  //   ) {
  //     return Red;
  //   }
  //   if (
  //     this.Fill_Status_Yellow !== 0 &&
  //     this.Fill_Status_Yellow <= this.deviceData.percent_motor_remaining
  //   ) {
  //     return Yellow;
  //   }
  //   return Green;
  // }

  submitManualAlert() {
    if (!this.manualAlert) {
      alert("Alert can not be empty");
      return;
    }
    this.blockUI.start("Please wait...");
    const date = new Date();
    const headers = new HttpHeaders({
      Authorization: "Basic YWRtaW46T0RBek1rNXZkbUZUWTI5MGFXRT0=",
    });
    this.http
      .post(
        "https://cloudfeedgrain-dev.mybluemix.net/device/alert_users",
        {
          device_id: this.deviceId.split(":")[1].trim(),

          message: this.manualAlert,
          alarm: 14,
          timestamp_str: date, //make this dynamic
        },
        { headers }
      )
      .subscribe((data: any) => {
        this.getAlerts(this.deviceId);
        this.blockUI.stop();
        alert("You alert is successfully submitted");
        this.manualAlert = "";
      });
  }
}
