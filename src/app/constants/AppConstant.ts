export class AppConstant {
  public static webAppName = "CapaciTrac, LLC";
  public static highColor = "green";
  public static mediumColor = "yellow";
  public static lowColor = "red";
  public static highFloor = 51;
  public static mediumFloor = 50;
  public static mediumCeil = 40;
  public static lowCeil = 24;
}
